# Changelog


## v0.2.5

[compare changes](https://gitlab.com/aa900031/eslint-config/compare/v0.2.4...v0.2.5)

### 🏡 Chore

- Update deps ([3538b33](https://gitlab.com/aa900031/eslint-config/commit/3538b33))
- **ci:** Update node 18 ([3993fb8](https://gitlab.com/aa900031/eslint-config/commit/3993fb8))

### ❤️ Contributors

- Zhong666 ([@aa900031](http://github.com/aa900031))

## v0.2.4

[compare changes](https://undefined/undefined/compare/v0.2.3...v0.2.4)

### 🏡 Chore

- Fix lint (449359c)

### ❤️  Contributors

- Zhong666 ([@aa900031](http://github.com/aa900031))

## v0.2.3

[compare changes](https://undefined/undefined/compare/v0.2.2...v0.2.3)


### 🏡 Chore

  - Update deps (c52c2b7)

### ❤️  Contributors

- Zhong666 ([@aa900031](http://github.com/aa900031))

## v0.2.2

[compare changes](https://undefined/undefined/compare/v0.2.1...v0.2.2)


### 🏡 Chore

  - Update deps (4508c22)

### ❤️  Contributors

- Zhong666 ([@aa900031](http://github.com/aa900031))

## v0.2.1

[compare changes](https://undefined/undefined/compare/v0.2.0...v0.2.1)


### 🩹 Fixes

  - Indent (9804c04)

### ❤️  Contributors

- Zhong666 ([@aa900031](http://github.com/aa900031))

## v0.2.0

[compare changes](https://undefined/undefined/compare/v0.1.1...v0.2.0)


### 🏡 Chore

  - 指定 pnpm (3acda24)
  - 更新依賴 (04b9d46)

### ❤️  Contributors

- Zhong666 ([@aa900031](http://github.com/aa900031))

## v0.1.1


### 🚀 Enhancements

  - Vue indent (45e8179)

### ❤️  Contributors

- Zhong666 <aa900031@gmail.com>

## v0.1.0


### 🚀 Enhancements

  - Indent tabs (d004537)

### 🏡 Chore

  - Init (0db7bdb)

### 🤖 CI

  - Add publish npm task (9585a47)

### ❤️  Contributors

- Zhong666 <aa900031@gmail.com>

